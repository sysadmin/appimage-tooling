#!/bin/bash
#
# Copyright (C) 2016 Scarlett Clark <sgclark@kde.org>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) version 3, or any
# later version accepted by the membership of KDE e.V. (or its
# successor approved by the membership of KDE e.V.), which shall
# act as a proxy defined in Section 6 of version 3 of the license.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License fo-r more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library.  If not, see <http://www.gnu.org/licenses/>.
export HOME=/var/jenkins_home
export PATH=/opt/usr/bin:$HOME/.rbenv/bin:$HOME/.rbenv/shims:$PATH
export RBENV_VERSION="2.5.0"
export RBENV_ROOT=$HOME/.rbenv
rbenv rehash
gem install --no-ri --no-rdoc bundler && rbenv rehash
sudo chown -R jenkins.jenkins /in
cd /in
eval "$(rbenv init -)"

bundle install
bundle show rspec
bundle list --paths
bundle env

bundle exec ruby tests/tooling.rb
