#!/usr/bin/env ruby
# frozen_string_literal: true
#
# Copyright (C) 2016 Scarlett Moore <sgclark@kde.org>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) version 3, or any
# later version accepted by the membership of KDE e.V. (or its
# successor approved by the membership of KDE e.V.), which shall
# act as a proxy defined in Section 6 of version 3 of the license.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library.  If not, see <http://www.gnu.org/licenses/>.
require 'tty-command'
require 'fileutils'

# Run the builds
class Build
  attr_accessor :pre
  attr_accessor :status
  def initialize(working_dir, need_patches = false, insource = false)
    unless File.exist?(working_dir)
      puts 'The source directory does not exist, something is terribly wrong.'
    end
    Dir.chdir(working_dir)
    if need_patches == true
      FileUtils.cp_r('/in/patches/.', "#{working_dir}/patches")
    end
    @pre = './'
    return unless insource == false

    Dir.mkdir('build')
    Dir.chdir('build')
    @pre = '../'
  end

  def run_build(type, options = '', profile = '')
    puts type
    puts options
    case type
    when 'cmake'
      build_cmake(options)
      build_make
      @status = $CHILD_STATUS
    when 'qmake'
      build_qmake(options, profile)
      build_make
      @status = $CHILD_STATUS
    when 'make'
      build_configure(options)
      build_make
      @status = $CHILD_STATUS
    when 'autoconf'
      build_autoconf
      build_configure(options)
      build_make
      @status = $CHILD_STATUS
    when 'autogen'
      build_autogen(options)
      build_configure(options)
      build_make
      @status = $CHILD_STATUS
    when 'custom'
      build_custom(options)
      @status = $CHILD_STATUS
    else
      puts "You gave me #{type} -- I am not configured for that."
    end
  end

  def build_cmake(options)
    puts 'cmake ' + options
    puts @pre
    cmd = TTY::Command.new
    if @pre == '../'
      cmd.run('cmake ../ ' + options)
    else
      cmd.run('cmake ' + options)
    end
    @status = $CHILD_STATUS
    puts @status
  end

  def build_qmake(options = null, profile)
    puts 'qmake ' + options.to_s
    Open3.popen2('qmake', options.to_s, @pre + profile) do |stdin, stdout, status_thread|
      stdout.each_line do |line|
        puts "LINE: #{line}"
      end
      raise 'QMake failed' unless status_thread.value.success?

      @status = $CHILD_STATUS
    end
  end

  def build_autogen(options = null)
    puts @pre + 'autogen.sh ' + options.to_s
    cmd = TTY::Command.new
    cmd.run('ls')
    cmd.run(@pre + 'autogen.sh ' + options.to_s)
    @status = $CHILD_STATUS
  end

  def build_configure(options = null)
    puts 'configure ' + options.to_s
    cmd = TTY::Command.new
    cmd.run(@pre + 'configure ' + options.to_s)
    @status = $CHILD_STATUS
    puts @status
  end

  def build_make
    cmd = TTY::Command.new
    cmd.run('make VERBOSE=1 -j 8')
    @status = $CHILD_STATUS
    puts @status
    cmd = TTY::Command.new
    cmd.run('make install')
    @status = $CHILD_STATUS
    puts @status
  end


  def run_autoconf
    cmd = TTY::Command.new
    cmd.run('aclocal')
    @status = $CHILD_STATUS
    puts @status
    cmd.run('automake --add-missing')
    @status = $CHILD_STATUS
    puts @status
    cmd.run('autoconf')
    @status = $CHILD_STATUS
    puts @status
  end

  def build_autoconf
    unless File.exist?('configure')
      if @pre == '../'
        Dir.chdir('../') do
          run_autoconf
          return
        end
      end
    else
      run_autoconf
      return
    end
    if File.exist?('configure')
      Open3.popen2('autoreconf', '--force', '--install', @pre) do |stdin, stdout, status_thread|
        stdout.each_line do |line|
          puts "LINE: #{line}"
        end
        raise 'Autoconf failed' unless status_thread.value.success?

        @status = $CHILD_STATUS
      end
    end
  end

  def build_custom(command)
    puts command
    cmd = TTY::Command.new
    cmd.run(command)
    @status = $CHILD_STATUS
  end

  def apply_patch(patch)
    puts "Applying #{patch}"
    cmd = TTY::Command.new
    if @pre == '../'
      Dir.chdir('../') do
        cmd.run('patch -p1 < ' + patch)
        return
      end
    else
    cmd.run('patch -p1 < ' + patch)
    @status = $CHILD_STATUS
    end
  end
end

#   def run_build(buildsystem, options, path, autoreconf, insource)
#       when 'autogen'
#         cmd = ''
#         Dir.chdir("#{path}") do
#           unless "#{insource}" == true
#             cmd =  "mkdir #{name}-builddir && cd #{name}-builddir && "
#           end
#           if "#{insource}" == true
#             cmd = ""
#           end
#           unless "#{autoreconf}" == true
#             cmd = cmd + "sh ../autogen.sh && ../configure --prefix=/opt/usr #{options} && make VERBOSE=1 -j 8 && make install"
#           end
#           if "#{autoreconf}" == true
#             cmd = cmd + "autoreconf --force --install && mkdir #{name}-builddir && cd #{name}-builddir && ../configure --prefix=/opt/usr #{options} &&  make VERBOSE=1 -j 8 && make install prefix=/opt/usr"
#           end
#           p "Running " + cmd
#           system(cmd)
#           FileUtils.rm_rf("#{name}-builddir", secure: true)
#           end
#           $?.exitstatus
#       when 'bootstrap'
#         Dir.chdir(path) do
#           p "running ./bootstrap #{options}"
#           system("./bootstrap #{options}")
#           system('make VERBOSE=1 -j 8 && make install')
#         end
#         $?.exitstatus
#       else
#       "You gave me #{buildsystem} -- I have no idea what to do with that."
#       end
#     end
# end
