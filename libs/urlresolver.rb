require 'uri'
require 'net/http'
require 'openssl'

# Resolve http urls
class UrlResolver
  def resolve(uri_str, agent = 'curl/7.43.0', max_attempts = 10, timeout = 10)
    attempts = 0

    until attempts >= max_attempts
      attempts += 1

      url = URI.parse(uri_str)
      http = Net::HTTP.new(url.host, url.port)
      http.open_timeout = timeout
      http.read_timeout = timeout
      puts url.path
      request = Net::HTTP::Get.new(url.path)

      if url.instance_of?(URI::HTTPS)
        http.use_ssl = true
        http.verify_mode = OpenSSL::SSL::VERIFY_PEER
        http.ca_file = File.join(File.dirname(__FILE__), 'cacert.pem')
      end
      response = http.request(request)
      puts response['Location']
      case response
      when Net::HTTPSuccess then
        puts 'Success'
        break
      when Net::HTTPRedirection then
        puts 'Redirected'
        location = response['Location']
        new_uri = URI.parse(location)
        uri_str = new_uri.to_s
      else
        raise 'Unexpected response: ' + response.inspect
      end

    end
    raise 'Too many http redirects' if attempts == max_attempts

    response.body
  end
end
