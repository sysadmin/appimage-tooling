#!/usr/bin/env ruby
# frozen_string_literal: true
#
# Copyright (C) 2016 Scarlett Moore <sgclark@kde.org>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) version 3, or any
# later version accepted by the membership of KDE e.V. (or its
# successor approved by the membership of KDE e.V.), which shall
# act as a proxy defined in Section 6 of version 3 of the license.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library.  If not, see <http://www.gnu.org/licenses/>.
require_relative '../libs/recipe'
require_relative '../libs/sources'
require_relative '../libs/setup'
require_relative '../libs/build'
require 'yaml'
require 'tty-command'

metadata = YAML.load_file('/in/data/metadata.yml')
puts metadata

describe Setup do
  describe 'initialize' do
    it 'Sets the project variables' do
      setup = Setup.new('/in/data/metadata.yml')
      setup.set_main_vars
      expect(setup.name).to eq(metadata['name']), " setup.name does not match metadata['name']"
    end
    it 'Sets the project environment' do
      setup = Setup.new('/in/data/metadata.yml')
      setup.set_env
      expect('/opt/usr/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin').to eq(ENV.fetch('PATH')), " PATH does not match"
    end
    it 'Installs the projects apt packages' do
      setup = Setup.new('/in/data/metadata.yml')
      setup.set_main_vars
      setup.install_packages(setup.packages)
      #expect(setup.status).to be(0), " Expected 0 exit Status"
    end
  end
end
describe Sources do
  describe 'Retrieve and build sources' do
    it 'Builds source dependencies that do not depend on kf5' do
      setup = Setup.new('/in/data/metadata.yml')
      setup.set_main_vars
      deps = setup.dependencies
      puts deps
      unless deps.nil?
        deps.each do |dep|
          puts dep
          setup = Setup.new('/in/data/metadata.yml')
          setup.set_dep_vars(dep)
          name = setup.depname
          type = setup.scm
          url = setup.url
          branch = setup.branch
          buildsystem = setup.buildsystem
          options = setup.buildoptions
          insource = setup.insource
          profile = setup.profile
          md5sum = setup.md5sum
          need_patches = setup.need_patches
          patches = setup.patches
          sources = Sources.new('/app/src')
          case type
          when 'tarball'
            file = File.basename(url)
            sources.fetch_tarball(url)
            sources.check_md5sum(file, md5sum)
            sources.unpack_tarball(file, name)
            expect(File.exist?("/app/src/#{name}")).to be(true), 'No sources'
          when 'git'
            sources.clone_git(url, branch)
            expect(File.exist?("/app/src/#{name}")).to be(true), 'No sources'
          end
          build = Build.new("/app/src/#{name}", need_patches, insource)
          cmd = TTY::Command.new
          cmd.run('pwd')
          setup.set_env
          if need_patches
            patches.each do |patch|
              build.apply_patch("patches/#{patch}")
            end
          end
          build.run_build(buildsystem, options, profile)
        end
      end
      if deps.nil?
        puts 'No dependencies'
      end
    end
    it 'Builds KDE Frameworks from source' do
      setup = Setup.new('/in/data/metadata.yml')
      setup.set_main_vars
      setup.set_env
      need = setup.build_kf5
      frameworks = setup.frameworks
      sources = Sources.new('/app/src')
      if need == true
        frameworks.each do |framework|
          puts framework
          name = framework.keys[0]
          puts name
          need_patches = framework['need_patches']
          patches = framework['patches']
          options = framework['options']
          url = "https://anongit.kde.org/#{name}"
          branch = 'master'
          sources.clone_git(url, branch)
          build = Build.new("/app/src/#{name}", need_patches, false)
          expect(Dir.exist?("/app/src/#{name}")).to be(true), "#{name} directory does not exist, something went wrong with source retrieval"
          if need_patches
            patches.each do |patch|
              build.apply_patch("patches/#{patch}")
            end
          end
          buildoptions = '-DCMAKE_INSTALL_PREFIX:PATH=/opt/usr -DKDE_INSTALL_SYSCONFDIR=/opt/etc \
           -DBUILD_TESTING=OFF ' + options
          build.run_build('cmake', buildoptions)
        end
      end
    end
  end
end

describe Recipe do
  describe 'generate_artifact' do
    it 'Tar things up for use with projects' do
      cmd = TTY::Command.new
      cmd.run('cd /artifacts && tar -zcvf artifact.tar.gz /opt')
      expect(Dir['/artifacts/*'].empty?).to be(false), 'No Artifact'
    end
  end
end
