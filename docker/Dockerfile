FROM ubuntu:16.04
RUN echo "deb http://us.archive.ubuntu.com/ubuntu/ xenial main restricted universe multiverse" >> /etc/apt/sources.list
RUN echo "deb-src http://ca.archive.ubuntu.com/ubuntu/ xenial main restricted universe multiverse" >> /etc/apt/sources.list
RUN echo "deb http://ca.archive.ubuntu.com/ubuntu/ xenial-updates main restricted universe multiverse" >> /etc/apt/sources.list
RUN echo "deb-src http://ca.archive.ubuntu.com/ubuntu/ xenial-updates main restricted universe multiverse" >> /etc/apt/sources.list
RUN echo "deb http://us.archive.ubuntu.com/ubuntu/ xenial-backports main restricted universe multiverse" >> /etc/apt/sources.list
RUN echo "deb-src http://us.archive.ubuntu.com/ubuntu/ xenial-backports main restricted universe multiverse" >> /etc/apt/sources.list

#link to bash
RUN rm /bin/sh && ln -s /bin/bash /bin/sh
RUN ln -s -f /bin/true /usr/bin/chfn
#tooling dependencies
RUN apt-get update && apt-get -y install wget git libreadline-dev cmake sudo \
bzip2 libssl-dev zlib1g-dev pkg-config
# Create jenkins user
RUN groupadd -g 127 jenkins
#Must match host.
RUN useradd -m -u 120 -g 127 -d /var/jenkins_home jenkins
RUN mkdir -p /{artifacts,appimages,app.Dir,source,app}
RUN chown jenkins.jenkins /appimages && chown jenkins.jenkins /app.Dir &&  chown jenkins.jenkins /source && \
chown jenkins.jenkins /artifacts && chown jenkins.jenkins /opt && chown jenkins.jenkins /app
RUN adduser jenkins sudo
RUN echo 'jenkins ALL=NOPASSWD: ALL' >> /etc/sudoers
RUN echo 'bundler ALL=NOPASSWD: /var/jenkins_home/.rbenv/bin/bundler' >> /etc/sudoers
# Cpan perl lib needed by kf5
RUN cd $SOURCES && cpan URI::Escape
# Everythin else as jenkins user
USER jenkins

 #Set ENV
ENV LD_LIBRARY_PATH /opt/usr/lib:/opt/usr/lib/gstreamer-1.0:/opt/usr/lib/x86_64-linux-gnu:/usr/lib:/usr/lib64:/lib
ENV PATH /opt/usr/bin:/var/jenkins_home/.rbenv/bin:/var/jenkins_home/.rbenv/shims:$PATH
ENV PKG_CONFIG_PATH /opt/usr/lib/pkgconfig:/opt/usr/lib/x86_64-linux-gnu/pkgconfig:/usr/lib/pkgconfig:/usr/share/pkgconfig:/usr/lib/x86_64-linux-gnu/pkgconfig
ENV CXXFLAGS -std=c++11 -fPIC
ENV SOURCES /source

 # Install rbenv and ruby-build
RUN echo 'gem: --no-rdoc --no-ri' >> ~/.gemrc
RUN git clone https://github.com/sstephenson/rbenv.git /var/jenkins_home/.rbenv
RUN git clone https://github.com/sstephenson/ruby-build.git /var/jenkins_home/.rbenv/plugins/ruby-build
RUN git clone git://github.com/carsomyr/rbenv-bundler.git /var/jenkins_home/.rbenv/plugins/bundler
RUN rbenv install -v 2.5.0 && rbenv global 2.5.0 && rbenv rehash && gem install bundler && rbenv rehash
RUN mkdir /var/jenkins_home/.bundle
RUN echo "BUNDLE_PATH: ~/vendor/bundle" >> /var/jenkins_home/.bundle/config
RUN echo 'BUNDLE_DISABLE_SHARED_GEMS: "1"' >> /var/jenkins_home/.bundle/config
RUN echo 'eval "$(rbenv init -)"' >> /var/jenkins_home/.bashrc && rbenv init -
ADD Gemfile /var/jenkins_home/Gemfile
RUN cd /var/jenkins_home &&  bundle install --binstubs && bundle show rspec

# QT5 and KF5 dependencies
RUN sudo apt-get update && sudo apt-get -y --force-yes install \
'^libxcb.*-dev' \
 libhyphen-dev \
autoconf \
autopoint \
bzr \
clang-3.8 \
docbook-xml \
docbook-xsl \
doxygen \
festival \
festival-dev \
fftw-dev \
firebird-dev \
fonts-noto \
freeglut3-dev \
gobject-introspection \
graphviz \
gtk-doc-tools \
guile-2.0-dev \
intltool \
libatkmm-1.6-dev \
libatomic-ops-dev \
libattr1-dev \
libboost-all-dev \
libbz2-dev \
libcap-dev \
libclang-3.8-dev \
libdbus-glib-1-dev \
libdouble-conversion-dev \
libegl1-mesa-drivers \
libfftw3-dev \
libgegl-dev \
libgirepository1.0-dev \
libgif-dev \
libgit2-dev \
libglew-dev \
libglm-dev \
libgtk-3-dev \
libgudev-1.0-dev \
libical-dev \
libjansson-dev \
libldap2-dev \
liblmdb-dev \
liblzma-dev \
libndp-dev \
libnl-3-dev \
libnm-dev \
libnss3-dev \
libopus-dev \
libpolkit-agent-1-dev \
libpolkit-backend-1-dev \
libpolkit-gobject-1-dev \
libqrencode-dev \
libsasl2-dev \
libsndfile1-dev \
libsystemd-dev \
libtool \
libunistring-dev \
libunwind8-dev \
libvorbis-dev \
libwebp-dev \
libxapian-dev \
libxerces-c-dev \
iptables-dev \
lldb-3.8 \
llvm-3.8 \
llvm-3.8-dev \
ml-lex \
modemmanager-dev \
mysql-client-5.7 \
mysql-server-core-5.7 \
postgresql \
postgresql-contrib \
postgresql-server-dev-all \
ppp-dev \
python-dev \
python3-dev \
software-properties-common \
subversion \
texinfo \
transfig \
unzip \
uuid-dev \
x11proto-dri3-dev \
x11proto-present-dev \
xsdcxx \
xsltproc \
yasm \
zsync

RUN sudo apt-get -y remove '.*wayland.*'
RUN sudo add-apt-repository -y ppa:wayland.admin/daily-builds && sudo apt-get update && sudo apt-get -y install libinput-dev  libwayland-dev \
 libwayland-server0 libwayland0 libegl1-mesa-dev libwayland-egl1-mesa libwayland-egl1 && sudo add-apt-repository -y -r ppa:wayland.admin/daily-builds
RUN sudo add-apt-repository -y ppa:brightbox/ruby-ng-experimental && sudo apt update && sudo apt-get -y install ruby2.5 ruby2.5-dev
RUN sudo apt-get update && sudo apt-get -y upgrade && sudo apt-get -y build-dep qt5-default
RUN sudo apt-get update && sudo apt-get -y build-dep libqt5webkit5-dev
RUN sudo add-apt-repository -y -r ppa:brightbox/ruby-ng-experimental
RUN sudo apt-get -y remove '.*qt.*'
